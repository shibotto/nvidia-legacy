# Nvidia 340 for Gentoo

The purpose of this overlay is to provide Nvidia proprietary driver 340 branch on top of up-to-date Gentoo, although with the following restrictions:

* Linux ≤ 6.6
* xorg-server ≤ 21.1

These requirements may change in the future, but please keep in mind that at some point it will be too complex or just plain impossible to keep up with upstream updates.

### !!! SECURITY WARNING !!!

Nvidia 340.108 was released back in December 2019 as the last version of the 340 branch. By now it is probably affected by serious security vulnerabilities which will never be fixed, therefore if security is your priority it is strongly recommended to switch to [Nouveau](https://wiki.gentoo.org/wiki/Nouveau).

## Adding the overlay

    mkdir -p /etc/portage/repos.conf
    wget -P /etc/portage/repos.conf/ https://gitlab.com/shibotto/nvidia-legacy/-/raw/master/nvidia-legacy.conf
    emaint sync --repo nvidia-legacy

Adjust the overlay configuration in `/etc/portage/repos.conf/nvidia-legacy.conf` to suit your need.

## Installation

Set `VIDEO_CARDS="nvidia"` in `/etc/portage/make.conf` then update the system with:

    emerge --changed-use --deep @world

This will pull in x11-drivers/nvidia-drivers (libraries and tools) and x11-drivers/nvidia-kmod (the kernel module).

Keep in mind that every time you install a new kernel, nvidia-kmod needs to be reinstalled as well. You can do that with `emerge @module-rebuild` if you are using a sys-kernel/\*-sources or add `USE="dist-kernel"` to your /etc/portage/make.conf if you are using a sys-kernel/\*-kernel\*.

## Configuration

### Permissions

Users needing to access the video card need to be in the `video` group:

    gpasswd -a <your_user> video

### Advanced configuration

This should get you up and running, if your card requires extra tweaks take a look at Gentoo's [NVIDIA/nvidia-drivers](https://wiki.gentoo.org/wiki/NVIDIA/nvidia-drivers) wiki page.
